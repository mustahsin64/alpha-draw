//
//  DrawLetterVC.swift
//  Alpha Draw
//
//  Created by Siam on 4/25/20.
//  Copyright © 2020 Siam. All rights reserved.
//

import UIKit
import FirebaseMLVision

class DrawLetterVC: UIViewController {
    
    var drawingLetter:String = ""
    var textRecognizer:VisionTextRecognizer!
    var alphabetList:[String] = []
    var currentIndex = 0

    @IBOutlet var targetLetter: UILabel!
    @IBOutlet var canvas: Canvas!
    @IBOutlet var confirmationImage: UIImageView!
    @IBOutlet var confirmationText: UILabel!
    @IBOutlet var retryButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup()
    {
        targetLetter.text = drawingLetter
        
        let vision = Vision.vision()
        textRecognizer = vision.onDeviceTextRecognizer()
    }
    
    //MARK:IBA
    @IBAction func undo(_ sender: Any) {
        canvas.undo()
    }
    @IBAction func redo(_ sender: Any) {
        canvas.redo()
    }
    @IBAction func clear(_ sender: Any) {
        canvas.clear()
    }
    @IBAction func confirm(_ sender: Any) {
        
        if let drawnImage = self.image(with: self.canvas)
        {
            runTextRecognization(image: drawnImage)
        }
        
    }
    
    @IBAction func retryAction(_ sender: Any) {
        self.retry()
    }
    @IBAction func nextClicked(_ sender: Any) {
        currentIndex += 1
        currentIndex = currentIndex%26
        
        drawingLetter = alphabetList[currentIndex]
        targetLetter.text = drawingLetter
        
        retry()
    }
    
    @IBAction func previuosClicked(_ sender: Any) {
        if(currentIndex > 0)
        {
            currentIndex -= 1
            currentIndex = currentIndex%26
        }
        else
        {
            currentIndex = 25
        }
        
        drawingLetter = alphabetList[currentIndex]
        targetLetter.text = drawingLetter
        
        retry()
    }
    
    //MARK: text recognization methods
    func runTextRecognization(image:UIImage)
    {
        let visionImage = VisionImage(image: image)
        textRecognizer.process(visionImage) { (features, error) in
            self.processResult(from: features, error: error)
        }
    }
    
    func processResult(from text:VisionText?, error:Error?)
    {
        guard let features = text else {
            self.showWrong(userInput: "No Alphabet is drawn")
            return
        }
        for block in features.blocks{
            for line in block.lines{
                for element in line.elements{
                    print(element)
                    let drawnText = element.text
                    if(drawnText == drawingLetter)
                    {
                        self.showCorrect()
                    }
                    else
                    {
                        self.showWrong(userInput: drawnText)
                    }
                    print("")
                    return
                }
            }
            
        }
    }
    
    //MARK: internal method
    func showCorrect()
    {
        confirmationImage.isHidden = false
        confirmationImage.image = UIImage(named: "right-mark")
        
        self.confirmationText.isHidden = true
        retryButton.isHidden = false
    }
    
    func showWrong(userInput:String)
    {
        confirmationImage.isHidden = false
        confirmationImage.image = UIImage(named: "cross-mark")
        
        if(userInput.count == 1)
        {
            self.confirmationText.text = "You have drawn \(userInput)"
            self.confirmationText.isHidden = false
        }
        retryButton.isHidden = false
    }
    
    func retry()
    {
        confirmationImage.isHidden = true
        self.confirmationText.isHidden = true
        confirmationImage.image = nil
        
        canvas.clear()
        retryButton.isHidden = true
    }
    
    func image(with view: UIView) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.isOpaque, 0.0)
        defer { UIGraphicsEndImageContext() }
        if let context = UIGraphicsGetCurrentContext() {
            view.layer.render(in: context)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            return image
        }
        return nil
    }
    
}
